const onRenderCallback = (
    id, // the "id" prop of the Profiler tree that has just committed
    phase, // either "mount" (if the tree just mounted) or "update" (if it re-rendered)
    actualDuration, // time spent rendering the committed update
    baseDuration, // estimated time to render the entire subtree without memoization
    startTime, // when React began rendering this update
    commitTime, // when React committed this update
    interactions,
) => {
    console.log('--------- logProfile fired -----------');
    console.log(`${id}'s ${phase.toUpperCase()} phase:`);
    console.log(`Actual time: ${actualTime} ms`);
    console.log(`Base time: ${baseTime} ms`);
    console.log(
        `Start time (since component mounted): ${startTime} ms`,
    );
    console.log(
        `Commit time (since component mounted): ${commitTime} ms`,
    );
    console.log(interactions);
};

export default onRenderCallback;
