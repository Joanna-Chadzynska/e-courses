import React, { createContext, useContext, useState } from 'react';

export const AppContext = createContext();

const AppProvider = ({ children }) => {
    const [isUserLogged, setIsUserLogged] = useState(false);

    const toggleLoggedState = () => setIsUserLogged((prev) => !prev);

    return (
        <AppContext.Provider
            value={{
                isUserLogged,
                toggleLoggedState,
            }}
        >
            {children}
        </AppContext.Provider>
    );
};

export const useAppContext = () => {
    const { isUserLogged, toggleLoggedState } = useContext(
        AppContext,
    );

    return { isUserLogged, toggleLoggedState };
};

export default AppProvider;
