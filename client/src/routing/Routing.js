import * as ROUTES from 'constants/routes';
import {
    About,
    Contact,
    Home,
    NotFound,
    SignIn,
    SignUp,
} from 'pages';
import React from 'react';
import {
    AnimatedRoutes,
    RouteTransition,
} from './animation/RouteTransition';

const Routing = () => {
    return (
        <AnimatedRoutes exitBeforeEnter initial={false}>
            <RouteTransition exact path={ROUTES.HOME} slideUp={30}>
                <Home />
            </RouteTransition>
            <RouteTransition slideUp={30} exact path={ROUTES.ABOUT}>
                <About />
            </RouteTransition>
            <RouteTransition slideUp={30} exact path={ROUTES.CONTACT}>
                <Contact />
            </RouteTransition>
            <RouteTransition slideUp={30} exact path={ROUTES.SIGN_IN}>
                <SignIn />
            </RouteTransition>
            <RouteTransition slideUp={30} exact path={ROUTES.SIGN_UP}>
                <SignUp />
            </RouteTransition>
            <RouteTransition slideUp={30} path="*">
                <NotFound />
            </RouteTransition>
        </AnimatedRoutes>
    );
};

export default Routing;
